# Welcome to my portfolio create with React & Next.js, deploy on Vercel, use Analytics, styled whith Chakra-ui, a React library commponents using Emotion

Find my portfolio of Conceptor Developer Web Desktop & Mobile Applications on url: 

`https://with-shallow-routing-rbwtm9gcf-hgouttebroze.vercel.app/`

"Welcome! I'm Hugues Simulacre G., I'm a french Conceptor Developer Web Desktop & Mobile Applications, living in Lyon city, France."

# Next.js, a React's framework

This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Use GitHub API to show my repositories

1. use the `isomorphic-fetch`'s dependencie & `getInitialProps`

```
import fetch from 'isomorphic-fetch';


export default class Gitub extends Component {
    static async getInitialProps() {
        const res = await fetch('https://api.github.com/users/gouttebroze/repos')
        const data = await res.json()
        return {repos:data}
    }
```
next, I use some informations of my repos like the name, languages uses...

```
import Link from "next/link";
import React, { Component } from "react";
import fetch from 'isomorphic-fetch';


export default class repo extends Component {
    static async getInitialProps({query}) {
        const res = await fetch(`https://api.github.com/repos/${query.user}/${query.repo}`)
        const data = await res.json()
        return {repo:data,user:query.user}
   }
    render() {
        return(
            <div>
                <Link href={`/github?user=${this.props.user}`} as={`/github/${this.props.user}`}>
                    <a>Back</a>
                </Link>
                <p>Name: {this.props.repo.name}</p>
                <p>Fork Count: {this.props.repo.forks_count}</p>
                <p>Language: {this.props.repo.language}</p>            
            </div>
        )
    }
}
```
## Deploy on Vercel

I have deploy this Next.js Application on Vercel, by GitHub.
On every git's push commit, GitLab send a pipline before deploy the code on Versel.

One of the benefist of using Versel with Next.js is that you can use easily Analytic.

The easiest way to deploy your Next.js app is to use the [Vercel Platform](https://vercel.com/new?utm_medium=default-template&filter=next.js&utm_source=create-next-app&utm_campaign=create-next-app-readme) from the creators of Next.js.

Check out our [Next.js deployment documentation](https://nextjs.org/docs/deployment) for more details.

## Use Analytic

On Versel...

## Getting Started

First, run the development server:

```bash
npm run dev
# or
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

You can start editing the page by modifying `pages/index.js`. The page auto-updates as you edit the file.

[API routes](https://nextjs.org/docs/api-routes/introduction) can be accessed on [http://localhost:3000/api/hello](http://localhost:3000/api/hello). This endpoint can be edited in `pages/api/hello.js`.

The `pages/api` directory is mapped to `/api/*`. Files in this directory are treated as [API routes](https://nextjs.org/docs/api-routes/introduction) instead of React pages.

## Learn More

To learn more about Next.js, take a look at the following resources:

- [Next.js Documentation](https://nextjs.org/docs) - learn about Next.js features and API.
- [Learn Next.js](https://nextjs.org/learn) - an interactive Next.js tutorial.

You can check out [the Next.js GitHub repository](https://github.com/vercel/next.js/) - your feedback and contributions are welcome!




## CHAKRA-UI library REACT components

1. install with yarn:
`yarn add @chakra-ui/react @emotion/react @emotion/styled framer-motion`

2. Wrap App with ChakraProvider:

```
import { ChakraProvider } from "@chakra-ui/react";

function MyApp({ Component, pageProps }) {
  return (
  <ChakraProvider>
    <Component {...pageProps} />
  </ChakraProvider>
)
}
```
# Use Chakra-ui

## use icons

inport chakra-ui icons: 
`yarn add @chakra-ui/icons`

# Next.js

## Head element 

For every page you can inject elements into the page head. This way you can add stylesheets, JS scripts, meta tags, a custom title or whatever you think is convenient to add inside the <head> of your page.

I have add in `pages/index.js` a title and a couple of meta tags.


