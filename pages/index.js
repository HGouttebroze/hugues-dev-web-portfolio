import Head from 'next/head';
import DarkModeSwitch from '../components/DarkModeSwich';
import { 
  Stack, 
  HStack, 
  VStack, 
  DarkMode, 
  Box,
	Image,
	Badge,
	Text,
	Icon,
  Flex, 
  Spacer,
	useColorMode,
  Divider, 
  Heading, 
  Button, 
  ButtonGroup, 
  CircularProgress, 
  CircularProgressLabel, 
  List, 
  ListItem, 
  ListIcon, 
  OrderedList, 
  UnorderedList,
  Progress
} from "@chakra-ui/react";
import Layout from '../components/Layout';
import { skills, experiences, projects, logiciels, Frameworks, environnementWork, humanSkills } from '../profil';

export default function Home() {

  const { colorMode, toggleColorMode } = useColorMode();
	const bgColor = { light: 'gray.400', dark: 'gray.700' };
  const textColor = { light: 'gray.500', dark: 'gray.100' };
  
  return (
    <Layout>

      <Head />

      {/* <Head>     
        <title>Hugues Simulacre Gouttebroze developeur web&apos;s Portfolio and Blog 🤔</title>
        <meta charSet="utf-8" />
        <meta property="og:title" content="Hugues Simulacre Gouttebroze developeur web's Portfolio and Blog" key="title" />
        <meta property="og:type" content="website" />
        <meta
          property="og:url"
          content={`${process.env.API_URL}}`}
        /> 
      </Head>
      <Head>
        <meta property="og:title" content="Hugues Simulacre Gouttebroze developeur web's Portfolio and Blog" key="title" />
      </Head> */}

  
      {/* <DarkModeSwitch /> */}

      <DarkMode />

      <Stack
        as="main"
        align="center"  
      >
          <Flex
          flexDirection="column"
          maxWidth="700px"
          w={[300, 400, 560]}
          flexBasis={['auto', '45%']}
        >
          <Flex
            flexDirection="row"
            w="700px"
            pt={4}
            justify="space-between"
          >
            
          </Flex>
          <Heading
            as="h1"
            size="2xl"
            w={[300, 400, 560]}
          >
           🦎️Hugues Simulacre😱️ Gouttebroze's Portfolio👾️  and Blog  🙄️
          </Heading>

          {/* <Box
            as="button"
            p={4}
            color="white"
            fontWeight="bold"
            borderRadius="md"
            bgGradient="linear(to-r, red.500, yellow.500)"
            _hover={{
              bgGradient: "linear(to-r, teal.200, teal.400, green.400, green.600, green.800)",
              bgGradient="linear(to-l, #7928CA,#FF0080)"
            }}
            w={[300, 400, 560]}
          >
            {DarkMode ? "Yep" : "Yop"}
          </Box> */}

          <Text
            bgGradient="linear(to-r, red.500, yellow.500)"
            _hover={{
              bgGradient: "linear(to-r, teal.200, teal.400, green.400, green.600, green.800)",
            }}
            bgClip="text"
            fontSize="6xl"
            fontWeight="extrabold"
          >
            I'm Hugues Simulacre G., I'm a french Conceptor Developer Web Desktop & Mobile Applications, living in Lyon city, France.
          </Text>

            <Text 
              fontSize="xl"
              my={4}
            >
            
          </Text>

          {/* CircularProgress whitl dynamicly render from my skills in profil.js's file and List (based on Chakra UI Components) */}
          {/* <Flex
          flexDirection="raw"
          maxWidth="700px"
          flexBasis={['auto', '45%']}
          >   <Flex
            maxW='1000px'
            w={['90vw', '90vw', '90vw', '70vw']}
            direction={['column']}
            justify='center'
            bg={bgColor[colorMode]}
            boxShadow='md'
            rounded='lg'
            p='4'>
          <h1>Skills / Compétences techniques</h1>

          {
            skills.map(({skill,percentage}, i) => (
              <div key={i}>
                <h5>{skill}</h5>
                <CircularProgress value={percentage}  
            >
                  <CircularProgressLabel bgGradient="linear(to-r, red.500, yellow.500)">{percentage}%</CircularProgressLabel>
                </CircularProgress> */}
            {/* <Stack spacing={5}>
                  <Progress colorScheme="green" size="sm" value={20}>{percentage}%</Progress>
                  <Progress colorScheme="green" size="md" value={20} />
                  <Progress colorScheme="green" size="lg" value={20} />
                  <Progress colorScheme="green" height="32px" value={20} />
              </Stack>

                <Progress hasStripe value={64} />  */}
              {/* </div>
              
            ))
          }
          </Flex> */}

        
{/* 
        <Button 
          size="lg" 
          colorScheme="red" 
          mt="24px"
          w={[300, 400, 560]}
          color="black"
        >
          Hey Oh, Let's Go!
        </Button> */}

        
      {/* </Flex>  */}
      </Flex>
      <Divider orientation='horizontal' m='4' />
  <Flex
        maxW='1000px'
        w={['90vw', '90vw', '90vw', '70vw']}
        direction={['column', 'column', 'row', 'row']}
        justify='center'
        bg={bgColor[colorMode]}
        boxShadow='md'
        rounded='lg'
        p='4'>
        <Flex align='center' wrap='nowrap'>
          <Image src='/icon.png' />
          <Box mx='4'>
            {/* <Text as='h2' fontSize='xl' fontWeight='bold' mb='2'>
              Usability
            </Text> */}
            <Text as='h3' fontWeight='light' fontSize='lg'>
              Sometimes the simples things are the hardest to find. So we created
              a new line for everday life.
            </Text>
          </Box>
        </Flex>
        <Divider orientation='vertical' m='4' />
        <Flex align='center' wrap='nowrap'>
          <Image src='/icon2.png' />
          <Box m='4'>
            <Text as='h2' fontSize='xl' fontWeight='bold' mb='2'>
              {/* Parralax Effect */}
              <h1>Skills / Compétences techniques</h1>

          {
            skills.map(({skill,percentage}, i) => (
              <div key={i}>
                <h5>{skill}</h5>
                <CircularProgress value={percentage} color="green.400">
                  <CircularProgressLabel>{percentage}%</CircularProgressLabel>
                </CircularProgress>
  {/* 
              <Stack spacing={5}>
                  <Progress colorScheme="green" size="sm" value={20}>{percentage}%</Progress>
                  <Progress colorScheme="green" size="md" value={20} />
                  <Progress colorScheme="green" size="lg" value={20} />
                  <Progress colorScheme="green" height="32px" value={20} />
                </Stack>

                <Progress hasStripe value={64} />  */}
              </div>
              
            ))
          }
            </Text>
            <Text as='h3' fontWeight='light' fontSize='lg'>
              Sometimes the simples things are the hardest to find. So we created
              a new line for everday life.
            </Text>
          </Box>
        </Flex>
        <Divider orientation='vertical' m='4' />
        <Flex align='center' wrap='nowrap'>
          <Image src='/icon2.png' />
          <Box m='4'>
            <Text as='h2' fontSize='xl' fontWeight='bold' mb='2'>
              {/* Parralax Effect */}
              <h1>Skills / Compétences techniques</h1>

          {
            Frameworks.map(({skill, percentage}, i) => (
              <div key={i}>
                <h5>{skill}</h5>
                <CircularProgress value={percentage} color="orange.400">
                  <CircularProgressLabel>{percentage}%</CircularProgressLabel>
                </CircularProgress>
  {/* 
              <Stack spacing={5}>
                  <Progress colorScheme="green" size="sm" value={20}>{percentage}%</Progress>
                  <Progress colorScheme="green" size="md" value={20} />
                  <Progress colorScheme="green" size="lg" value={20} />
                  <Progress colorScheme="green" height="32px" value={20} />
                </Stack>

                <Progress hasStripe value={64} />  */}
              </div>
              
            ))
          }
            </Text>
            <Text as='h3' fontWeight='light' fontSize='lg'>
              Sometimes the simples things are the hardest to find. So we created
              a new line for everday life.
            </Text>
          </Box>
        </Flex>
        <Divider orientation='vertical' m='4' />
        <Flex align='center' wrap='nowrap'>
          <Image src='/icon3.png' />
          <Box mx='4'>
            <Text as='h2' fontSize='xl' fontWeight='bold' mb='2'>
              Unlimited Colors
            </Text>
            <Text as='h3' fontWeight='light' fontSize='lg'>
              Sometimes the simples things are the hardest to find. So we created
              a new line for everday life.
            </Text>
          </Box>
        </Flex>

        <Divider orientation='vertical' m='4' />
        <Flex align='center' wrap='nowrap'>
          <Image src='/icon2.png' />
          <Box m='4'>
            <Text as='h2' fontSize='xl' fontWeight='bold' mb='2'>
              {/* Parralax Effect */}
              <h1>Environnement de travail</h1>

          {
            environnementWork.map(({skill,percentage}, i) => (
              <div key={i}>
                <h5>{skill}</h5>
                <CircularProgress value={percentage} color="yellow.400">
                  <CircularProgressLabel>{percentage}%</CircularProgressLabel>
                </CircularProgress>
  {/* 
              <Stack spacing={5}>
                  <Progress colorScheme="green" size="sm" value={20}>{percentage}%</Progress>
                  <Progress colorScheme="green" size="md" value={20} />
                  <Progress colorScheme="green" size="lg" value={20} />
                  <Progress colorScheme="green" height="32px" value={20} />
                </Stack>

                <Progress hasStripe value={64} />  */}
              </div>
              
            ))
          }
            </Text>
            <Text as='h3' fontWeight='light' fontSize='lg'>
              Sometimes the simples things are the hardest to find. So we created
              a new line for everday life.
            </Text>
          </Box>
        </Flex>
        <Divider orientation='vertical' m='4' />
        <Flex align='center' wrap='nowrap'>
          <Image src='/icon2.png' />
          <Box m='4'>
            <Text as='h2' fontSize='xl' fontWeight='bold' mb='2'>
              {/* Parralax Effect */}
              <h1>Logiciels</h1>

          {
            logiciels.map(({skill, percentage}, i) => (
              <div key={i}>
                <h5>{skill}</h5>
                <CircularProgress value={percentage} color="purple.400">
                  <CircularProgressLabel>{percentage}%</CircularProgressLabel>
                </CircularProgress>
  {/* 
              <Stack spacing={5}>
                  <Progress colorScheme="green" size="sm" value={20}>{percentage}%</Progress>
                  <Progress colorScheme="green" size="md" value={20} />
                  <Progress colorScheme="green" size="lg" value={20} />
                  <Progress colorScheme="green" height="32px" value={20} />
                </Stack>

                <Progress hasStripe value={64} />  */}
              </div>
              
            ))
          }
            </Text>
            <Text as='h3' fontWeight='light' fontSize='lg'>
              Sometimes the simples things are the hardest to find. So we created
              a new line for everday life.
            </Text>
          </Box>
        </Flex>
        <Divider orientation='vertical' m='4' />
        <Flex align='center' wrap='nowrap'>
          <Image src='/icon2.png' />
          <Box m='4'>
            <Text as='h2' fontSize='xl' fontWeight='bold' mb='2'>
              {/* Parralax Effect */}
              <h1>Logiciels</h1>

          {
            logiciels.map(({skill, percentage}, i) => (
              <div key={i}>
                <h5>{skill}</h5>
                <CircularProgress value={percentage} color="purple.400">
                  <CircularProgressLabel>{percentage}%</CircularProgressLabel>
                </CircularProgress>
  {/* 
              <Stack spacing={5}>
                  <Progress colorScheme="green" size="sm" value={20}>{percentage}%</Progress>
                  <Progress colorScheme="green" size="md" value={20} />
                  <Progress colorScheme="green" size="lg" value={20} />
                  <Progress colorScheme="green" height="32px" value={20} />
                </Stack>

                <Progress hasStripe value={64} />  */}
              </div>
              
            ))
          }
            </Text>
            <Text as='h3' fontWeight='light' fontSize='lg'>
              Sometimes the simples things are the hardest to find. So we created
              a new line for everday life.
            </Text>
          </Box>
        </Flex>
        <Divider orientation='vertical' m='4' />

        <Flex align='center' wrap='nowrap'>
          <Image src='/icon2.png' />
          <Box m='4'>
            <Text as='h2' fontSize='xl' fontWeight='bold' mb='2'>
              {/* Parralax Effect */}
              <h1>Compétences humaines</h1>

          {
            humanSkills.map(({skill, percentage}, i) => (
              <div key={i}>
                <h5>{skill}</h5>
                <CircularProgress value={percentage} color="purple.800">
                  <CircularProgressLabel>{percentage}%</CircularProgressLabel>
                </CircularProgress>
  {/* 
              <Stack spacing={5}>
                  <Progress colorScheme="green" size="sm" value={20}>{percentage}%</Progress>
                  <Progress colorScheme="green" size="md" value={20} />
                  <Progress colorScheme="green" size="lg" value={20} />
                  <Progress colorScheme="green" height="32px" value={20} />
                </Stack>

                <Progress hasStripe value={64} />  */}
              </div>
              
            ))
          }
            </Text>
            <Text as='h3' fontWeight='light' fontSize='lg'>
              Sometimes the simples things are the hardest to find. So we created
              a new line for everday life.
            </Text>
          </Box>
        </Flex>
        <Divider orientation='vertical' m='4' />
        
      </Flex>
    


        

      </Stack> 
    </Layout> 
  )
};
