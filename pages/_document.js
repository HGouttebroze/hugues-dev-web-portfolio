import Document, { Html, Head, Main, NextScript } from 'next/document';
import GoogleFonts from 'next-google-fonts';
import { ColorModeScript } from '@chakra-ui/react';

// class MyDocument extends Document {
//   static async getInitialProps(ctx) {
//     const initialProps = await Document.getInitialProps(ctx)
//     return { ...initialProps }
//   }

export default class MyDocument extends Document {

  render() {
    return (
      <Html lang="fr">
        {/* <GoogleFonts href="https://fonts.googleapis.com/css2?family=Indie+Flower&display=swap" rel="stylesheet" /> */}
        
        <Head />
        <body>
          <ColorModeScript />  
          <Main />
          <NextScript />
        </body>
      </Html>
    )
  }
}

