import Link from "next/link";
import React, { Component } from "react";
import fetch from 'isomorphic-fetch';
import Container from "../../components/Container";


export default class repo extends Component {
    static async getInitialProps({query}) {
        const res = await fetch(`https://api.github.com/repos/${query.user}/${query.repo}`)
        const data = await res.json()
        return {repo:data,user:query.user}
   }
    render() {
        return(
            <Container>
            <div>
                <Link href={`/github?user=${this.props.user}`} as={`/github/${this.props.user}`}>
                    <a>Back</a>
                </Link>
                <p>Name: {this.props.repo.name}</p>
                <p>Fork Count: {this.props.repo.forks_count}</p>
                <p>Language: {this.props.repo.language}</p>     
                <p>Description: {this.props.repo.description}</p>        
            </div>
          </Container>

           
          
        )
    }
}