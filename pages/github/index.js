import React, { Component } from "react"
import Layout from "../../components/Layout"
import fetch from 'isomorphic-fetch';
import Link from "next/link";


export default class Gitub extends Component {
    static async getInitialProps() {
        const res = await fetch('https://api.github.com/users/gouttebroze/repos')
        const data = await res.json()
        return {repos:data}
    }
    render() {
        
       return( 
           <Layout>
                <ul>
                    {
                        this.props.repos.map(repo => (
                            <Link href={`/github/repo?user=gouttebroze&repo=${repo.name}`}
                            as={`/github/gouttebroze/${repo.name}`}>
                            <a><li>{repo.name}</li></a>
                            </Link>
                        ))
                    }
                    
                </ul>
           </Layout>
       )
    }
}
