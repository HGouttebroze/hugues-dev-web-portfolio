export const skills = [
  {
    skill: "JavaScript",
    percentage: 70,
    experienceYears: 3
  },
  {
    skill: "HTML/CSS",
    percentage: 90,
    experienceYears: 4
  },
  {
    skill: "Node.js",
    percentage: 70,
    experienceYears: 2
  },
  {
    skill: "Java",
    percentage: 50,
    experienceYears: 1
  },

  {
    skill: "PHP",
    percentage: 40,
    experienceYears: 3
  },

  {
    skill: "SQL",
    percentage: 70,
    experienceYears: 3
  },
  {
    skill: "Git",
    percentage: 75,
    experienceYears: 3
  },
  {
    skill: "Linux",
    percentage: 70,
    experienceYears: 12
  },
  {
    skill: "SEO",
    percentage: 65,
    experienceYears: 3
  },
];

export const Frameworks = [
    
    {
      skill: "React",
      percentage: 75,
      experienceYears: 2
    },
    {
        skill: "React Native",
        percentage: 60,
        experienceYears: 2
      },

      {
        skill: "Redux",
        percentage: 50,
        experienceYears: 1
      },
    {
      skill: "Next.js",
      percentage: 85,
      experienceYears: 1
    },
    {
      skill: "GatsbyJS",
      percentage: 90,
      experienceYears: 2
    },
    {
      skill: "Vue.js",
      percentage: 70,
      experienceYears: 3
    },
    {
      skill: "Hibernate",
      percentage: 50,
      experienceYears: 1
    },
    {
      skill: "Spring",
      percentage: 50,
      experienceYears: 1
    },
    {
      skill: "Nuxt.js",
      percentage: 60,
      experienceYears: 1
    },
    {
      skill: "Laravel",
      percentage: 70,
      experienceYears: 3
    },
    {
        skill: "Express",
        percentage: 70,
        experienceYears: 2
      },
      {
        skill: "Synfony",
        percentage: 50,
        experienceYears: 1
      },
    {
      skill: "Angular",
      percentage: 50,
      experienceYears: 1
    },
    
  ];

  export const logiciels = [
    {
      skill: "Inskcape",
      description: "logiciel OpenSource de Dessin Vectoriel pour SVG",
      percentage: 80,
      experienceYears: 2
    },    
    {
      skill: "VSCodium",
      description: "IDE Opensource",
      percentage: 80,
      experienceYears: 4
    },
    {
      skill: "Eclipse",
      description: "IDE: utilisation pour Java, version Entreprise",
      percentage: 70,
      experienceYears: 1
    },
    {
        skill: "PhpMyAdmin",
        description: "utilisation pour gestion de Bases de Données sur MySQL",
        percentage: 90,
        experienceYears: 3
      },
    {
        skill: "HeidiSQL",
        description: "utilisation pour gestion de Bases de Données sur Maria DB",
        percentage: 90,
        experienceYears: 1
      },
      {
        skill: "WorkBench",
        description: "utilisation pour gestion de shéma UML de Bases de Données SQL",
        percentage: 90,
        experienceYears: 2
      }
    
  ];
  

  export const environnementWork = [ 
    
    {
      skill: "Linux",
      percentage: 80,
      experienceYears: 12
    },
    {
      skill: "Shell",
      percentage: 60,
      experienceYears: 4
    },
    {
      skill: "GitLab",
      percentage: 90,
      experienceYears: 3
    },
    {
        skill: "Integration Continue / YAML",
        percentage: 70,
        experienceYears: 2
    },
   
    {
      skill: "GitHub",
      percentage: 90,
      experienceYears: 4
    },
    {
      skill: "Pratiques DevOps",
      percentage: 70,
      experienceYears: 2
    },
    
    {
        skill: "Vercel/Analytics",
        percentage: 70,
        experienceYears: 1
      }
  ];

  export const CMSheadless = [
    {
      skill: "Stripe",
      percentage: 60,
      experienceYears: 1
    },
    {
      skill: "Wordpress",
      percentage: 60,
      experienceYears: 4
    },
   
    
  ];

  export const humanSkills = [
    {
      skill: "JavaScript",
      percentage: 60,
    },
    {
      skill: "React",
      percentage: 60,
    },
    {
      skill: "Next.js",
      percentage: 80,
    },
    {
      skill: "HTML/CSS",
      percentage: 90,
    },
    
  ];

export const experiences = [
  {
    title: "Developpeur Web",
    description:
      "Développeur/Intégrateur Web chez Time for the Planet: bénévolat (CSS: SASS, BEM. Ruby-On-Rails).",
    from: 2020,
    to: 2021,
  },
  {
    title: "Developpeur Web React/NodeJS",
    description:
        "Développeur Web chez Lydra, DevOps, chargé de l'intégration continue sous GitLab Pages et du développeur d'un site statique en JavaScript, sous GatsbyJS, un Framework React. Méthode SCRUM, en Sprint, utilisation de logiciels OpenSources, Travail avec Git, Linux, Insckape (dessin vectoriel: SVG), EsLint, Prettier, NodeJS, SASS, CSS-IN-JS...",
    from: 2020,
    to: 2020,
  },
  {
    title: "Software developer at SpaceX",
    description:
      "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Sint excepturi ea explicabo. Illum suscipit illo, porro quisquam voluptatem officiis fugiat vel animi aliquam inventore rem. Quo laudantium temporibus cupiditate. Aut?",
    from: 2010,
  },
];

export const diplomes = [
    {
      title: "Diplôme Universitaire de Developpeur Web Fullstack",
      description:
        "Développeur/Intégrateur Web chez Time for the Planet: bénévolat (CSS: SASS, BEM. Ruby-On-Rails).",
      from: 2020,
      to: 2021,
    },
    {
      title: "Developpeur Web React/NodeJS",
      description:
          "Développeur Web chez Lydra, DevOps, chargé de l'intégration continue sous GitLab Pages et du développeur d'un site statique en JavaScript, sous GatsbyJS, un Framework React. Méthode SCRUM, en Sprint, utilisation de logiciels OpenSources, Travail avec Git, Linux, Insckape (dessin vectoriel: SVG), EsLint, Prettier, NodeJS, SASS, CSS-IN-JS...",
      from: 2020,
      to: 2020,
    },
    {
      title: "Software developer at SpaceX",
      description:
        "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Sint excepturi ea explicabo. Illum suscipit illo, porro quisquam voluptatem officiis fugiat vel animi aliquam inventore rem. Quo laudantium temporibus cupiditate. Aut?",
      from: 2010,
    },
  ];

export const projects = [
  {
    name: "Awesome Website 1",
    description:
      "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Sint excepturi ea explicabo. Illum suscipit illo, porro quisquam voluptatem",
    image: "portfolio1.jpeg",
  },
  {
    name: "Awesome Website 2",
    description:
      "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Sint excepturi ea explicabo. Illum suscipit illo, porro quisquam voluptatem",
    image: "portfolio2.jpg",
  },
  {
    name: "Awesome Website 3",
    description:
      "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Sint excepturi ea explicabo. Illum suscipit illo, porro quisquam voluptatem",
    image: "portfolio3.png",
  },
  {
    name: "Awesome Website 4",
    description:
      "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Sint excepturi ea explicabo. Illum suscipit illo, porro quisquam voluptatem",
    image: "portfolio4.png",
  },
  {
    name: "Awesome Website 5",
    description:
      "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Sint excepturi ea explicabo. Illum suscipit illo, porro quisquam voluptatem",
    image: "portfolio5.jpeg",
  },
  {
    name: "Awesome Website 6",
    description:
      "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Sint excepturi ea explicabo. Illum suscipit illo, porro quisquam voluptatem",
    image: "portfolio6.jpeg",
  },
];

export const posts = [
  {
    title: "React",
    content:
      "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Sint excepturi ea explicabo. Illum suscipit illo, porro quisquam voluptatem",
    imageURL:
      "https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Faie.edu.au%2Fwp-content%2Fuploads%2F2018%2F03%2Fgame-programming-03.jpg",
  },
  {
    title: "Angular",
    content:
      "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Sint excepturi ea explicabo. Illum suscipit illo, porro quisquam voluptatem",
    imageURL:
      "https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Faie.edu.au%2Fwp-content%2Fuploads%2F2018%2F03%2Fgame-programming-03.jpg",
  },
  {
    title: "Nextjs",
    content:
      "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Sint excepturi ea explicabo. Illum suscipit illo, porro quisquam voluptatem",
    imageURL:
      "https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Faie.edu.au%2Fwp-content%2Fuploads%2F2018%2F03%2Fgame-programming-03.jpg",
  },
];

export const proj = [
    {
      name: "Awesome Website 1",
      description:
        "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Sint excepturi ea explicabo. Illum suscipit illo, porro quisquam voluptatem",
      image: "portfolio1.jpeg",
    },
    {
      name: "Awesome Website 2",
      description:
        "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Sint excepturi ea explicabo. Illum suscipit illo, porro quisquam voluptatem",
      image: "portfolio2.jpg",
    },
    {
      name: "Awesome Website 3",
      description:
        "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Sint excepturi ea explicabo. Illum suscipit illo, porro quisquam voluptatem",
      image: "portfolio3.png",
    },
    {
      name: "Awesome Website 4",
      description:
        "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Sint excepturi ea explicabo. Illum suscipit illo, porro quisquam voluptatem",
      image: "portfolio4.png",
    },
    {
      name: "Awesome Website 5",
      description:
        "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Sint excepturi ea explicabo. Illum suscipit illo, porro quisquam voluptatem",
      image: "portfolio5.jpeg",
    },
    {
      name: "Awesome Website 6",
      description:
        "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Sint excepturi ea explicabo. Illum suscipit illo, porro quisquam voluptatem",
      image: "portfolio6.jpeg",
    },
  ];
  
  export const p = [
    {
      title: "React",
      content:
        "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Sint excepturi ea explicabo. Illum suscipit illo, porro quisquam voluptatem",
      imageURL:
        "https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Faie.edu.au%2Fwp-content%2Fuploads%2F2018%2F03%2Fgame-programming-03.jpg",
    },
    {
      title: "Angular",
      content:
        "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Sint excepturi ea explicabo. Illum suscipit illo, porro quisquam voluptatem",
      imageURL:
        "https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Faie.edu.au%2Fwp-content%2Fuploads%2F2018%2F03%2Fgame-programming-03.jpg",
    },
    {
      title: "Nextjs",
      content:
        "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Sint excepturi ea explicabo. Illum suscipit illo, porro quisquam voluptatem",
      imageURL:
        "https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Faie.edu.au%2Fwp-content%2Fuploads%2F2018%2F03%2Fgame-programming-03.jpg",
    },
  ];

  export const prhhhh = [
    {
      name: "Awesome Website 1",
      description:
        "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Sint excepturi ea explicabo. Illum suscipit illo, porro quisquam voluptatem",
      image: "portfolio1.jpeg",
    },
    {
      name: "Awesome Website 2",
      description:
        "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Sint excepturi ea explicabo. Illum suscipit illo, porro quisquam voluptatem",
      image: "portfolio2.jpg",
    },
    {
      name: "Awesome Website 3",
      description:
        "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Sint excepturi ea explicabo. Illum suscipit illo, porro quisquam voluptatem",
      image: "portfolio3.png",
    },
    {
      name: "Awesome Website 4",
      description:
        "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Sint excepturi ea explicabo. Illum suscipit illo, porro quisquam voluptatem",
      image: "portfolio4.png",
    },
    {
      name: "Awesome Website 5",
      description:
        "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Sint excepturi ea explicabo. Illum suscipit illo, porro quisquam voluptatem",
      image: "portfolio5.jpeg",
    },
    {
      name: "Awesome Website 6",
      description:
        "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Sint excepturi ea explicabo. Illum suscipit illo, porro quisquam voluptatem",
      image: "portfolio6.jpeg",
    },
  ];
  
  export const pos = [
    {
      title: "React",
      content:
        "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Sint excepturi ea explicabo. Illum suscipit illo, porro quisquam voluptatem",
      imageURL:
        "https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Faie.edu.au%2Fwp-content%2Fuploads%2F2018%2F03%2Fgame-programming-03.jpg",
    },
    {
      title: "Angular",
      content:
        "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Sint excepturi ea explicabo. Illum suscipit illo, porro quisquam voluptatem",
      imageURL:
        "https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Faie.edu.au%2Fwp-content%2Fuploads%2F2018%2F03%2Fgame-programming-03.jpg",
    },
    {
      title: "Nextjs",
      content:
        "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Sint excepturi ea explicabo. Illum suscipit illo, porro quisquam voluptatem",
      imageURL:
        "https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Faie.edu.au%2Fwp-content%2Fuploads%2F2018%2F03%2Fgame-programming-03.jpg",
    },
  ];

  export const toDefine = [
    {
      name: "Awesome Website 1",
      description:
        "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Sint excepturi ea explicabo. Illum suscipit illo, porro quisquam voluptatem",
      image: "portfolio1.jpeg",
    },
    {
      name: "Awesome Website 2",
      description:
        "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Sint excepturi ea explicabo. Illum suscipit illo, porro quisquam voluptatem",
      image: "portfolio2.jpg",
    },
    {
      name: "Awesome Website 3",
      description:
        "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Sint excepturi ea explicabo. Illum suscipit illo, porro quisquam voluptatem",
      image: "portfolio3.png",
    },
    {
      name: "Awesome Website 4",
      description:
        "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Sint excepturi ea explicabo. Illum suscipit illo, porro quisquam voluptatem",
      image: "portfolio4.png",
    },
    {
      name: "Awesome Website 5",
      description:
        "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Sint excepturi ea explicabo. Illum suscipit illo, porro quisquam voluptatem",
      image: "portfolio5.jpeg",
    },
    {
      name: "Awesome Website 6",
      description:
        "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Sint excepturi ea explicabo. Illum suscipit illo, porro quisquam voluptatem",
      image: "portfolio6.jpeg",
    },
  ];
  
  export const Librairies= [
    {
      Framework: "Bootstrap",
      content:
        "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Sint excepturi ea explicabo. Illum suscipit illo, porro quisquam voluptatem",
      imageURL:
        "https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Faie.edu.au%2Fwp-content%2Fuploads%2F2018%2F03%2Fgame-programming-03.jpg",
        experienceYears: 2
    },
    {
    Framework: "Chakra-UI",
      content:
        "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Sint excepturi ea explicabo. Illum suscipit illo, porro quisquam voluptatem",
      imageURL:
        "https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Faie.edu.au%2Fwp-content%2Fuploads%2F2018%2F03%2Fgame-programming-03.jpg",
        experienceYears: 2
    },
    {
        Framework: "Emotion",
      content:
        "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Sint excepturi ea explicabo. Illum suscipit illo, porro quisquam voluptatem",
      imageURL:
        "https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Faie.edu.au%2Fwp-content%2Fuploads%2F2018%2F03%2Fgame-programming-03.jpg",
        experienceYears: 2
    },
    {
        Framework: "Styled-Components",
      content:
        "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Sint excepturi ea explicabo. Illum suscipit illo, porro quisquam voluptatem",
      imageURL:
        "https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Faie.edu.au%2Fwp-content%2Fuploads%2F2018%2F03%2Fgame-programming-03.jpg",
        experienceYears: 2
    },
    {
        Framework: "TailwindCSS",
        content:
          "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Sint excepturi ea explicabo. Illum suscipit illo, porro quisquam voluptatem",
        imageURL:
          "https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Faie.edu.au%2Fwp-content%2Fuploads%2F2018%2F03%2Fgame-programming-03.jpg",
          experienceYears: 2
      },
      {
      Framework: "Vuetify",
        content:
          "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Sint excepturi ea explicabo. Illum suscipit illo, porro quisquam voluptatem",
        imageURL:
          "https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Faie.edu.au%2Fwp-content%2Fuploads%2F2018%2F03%2Fgame-programming-03.jpg",
          experienceYears: 2
      },
      {
          Framework: "Houdini",
        content:
          "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Sint excepturi ea explicabo. Illum suscipit illo, porro quisquam voluptatem",
        imageURL:
          "https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Faie.edu.au%2Fwp-content%2Fuploads%2F2018%2F03%2Fgame-programming-03.jpg",
          experienceYears: 1
      },
  ];