import * as React from "react";
import NextHead from "next/head";
import { GoogleFonts } from "next-google-fonts";

export const Head = ({ children, title }) => (
  <React.Fragment>
    {/* <GoogleFonts href="https://fonts.googleapis.com/css2?family=Inter:wght@400;700&display=swap" /> */}
    <GoogleFonts href="https://fonts.googleapis.com/css2?family=Indie+Flower&display=swap" />
    <NextHead>
      <meta charSet="UTF-8" />
      <meta
        name="viewport"
        content="width=device-width, initial-scale=1.0"
      />
      <meta httpEquiv="x-ua-compatible" content="ie=edge" />

      <title>{title}</title>

      {children}
    </NextHead>
  </React.Fragment>
);
{/* 
<Head>     
<title>Hugues Simulacre Gouttebroze developeur web&apos;s Portfolio and Blog 🤔</title>
<meta charSet="utf-8" />
<meta property="og:title" content="Hugues Simulacre Gouttebroze developeur web's Portfolio and Blog" key="title" />
<meta property="og:type" content="website" />
<meta
  property="og:url"
  content={`${process.env.API_URL}}`}
/> 
</Head>
<Head>
<meta property="og:title" content="Hugues Simulacre Gouttebroze developeur web's Portfolio and Blog" key="title" />
</Head> 
*/}