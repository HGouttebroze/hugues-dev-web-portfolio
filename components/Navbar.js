import {
	Flex,
	Stack,
	PseudoBox,
	useColorMode,
	IconButton,
	Box,
    Image,
    Text
} from '@chakra-ui/react';
import Link from 'next/link';
//import { useRouter } from 'next/router';
import DarkModeSwitch from './DarkModeSwich';

 /*opacity={router.pathname !== '/form' ? 0.4 : 1} */

const Navbar = () => {
	const { colorMode, toggleColorMode } = useColorMode();
	const bgColor = { light: 'gray.300', dark: 'gray.600' };
	const textColor = { light: 'black', dark: 'gray.100' };
	//const router = useRouter();
	return (
		<Flex
			w='100vw'
			bg={bgColor[colorMode]}
			align='center'
			color={textColor[colorMode]}
			justify='center'
			align='center'
			fontSize={['md', 'lg', 'xl', 'xl']}
			h='7vh'
			boxShadow='md'
            p={2}
            flexBasis={['auto', '45%']}>
			<Flex w={['100vw', '100vw', '80vw', '80vw']} justify='space-around'>

                
				{/* <Box>
					<Image borderRadius="full" h='6vh' src='./Cruzada.png' alt='Simulacre Web Logo' />
				</Box>

				<Box>
					<Image borderRadius="full" h='6vh' src='./coder-bg-Img.png' alt='Simulacre Web Logo' />
				</Box> */}

				{/* <Box>
					<Image borderRadius="full" h='6vh' src='./unsplash.jpg' alt='Simulacre Web Logo' />
				</Box> */}

				<Box>
					<Image borderRadius="full" h='6vh' src='./logo-simulacre-bg-orange.png' alt='Simulacre Web Logo' />
				</Box> 

                

				<Stack
					spacing={8}
					color={textColor[colorMode]}
					justify='center'
					align='center'
					isInline>
					
						<Link href='/'>
							<a>Home</a>
						</Link>
					
					
						<Link href='/about'>
							<a>About</a>
						</Link>
					
						{/* <Link href='/card'>
							<a>Card</a>
						</Link> */}
					
                    <Link href="/github?user=gouttebroze" as="/github/gouttebroze">                        
                            <a>GitHub's Codes Repos</a>                  
                    </Link>
                        

						<Link href='/list'>
							<a>List</a>
						</Link>

                        <Link href='/articles'>
							<a>Posts</a>
						</Link>

                        <Link href='/contact'>
							<a>Contact</a>
						</Link>
				

				</Stack>
				{/* <Box>
					<IconButton
						rounded='full'
						onClick={toggleColorMode}
						icon={colorMode === 'light' ? 'moon' : 'sun'}>
						Change Color Mode
					</IconButton>
				</Box> */}
                <DarkModeSwitch />
			</Flex>
		</Flex>
	);
};

export default Navbar;