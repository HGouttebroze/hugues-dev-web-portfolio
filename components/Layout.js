import Link from 'next/link'
import Head from 'next/head'
//import Header from './Header';

import { Image } from "@chakra-ui/react"
import Navbar from './Navbar'

export default function Layout({
  children,
  title = "Hugues Simulacre Gouttebroze developeur web's Portfolio 🤔",
}) {
  return (
    <div>
      <Head>
        <title>{title}</title>
        <meta charSet="utf-8" />
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      </Head>
      <header>
      {/* <Header /> */}
        <nav>
          {/* <Link href="/">
            <a>Home</a>
          </Link>{' '}
          |
          <Link href="/about">
            <a>About</a>
          </Link>{' '}
          |
          <Link href="/contact">
            <a>Contact</a>
          </Link> */}

          {/* <Image
            borderRadius="full"
            boxSize="150px"
            h='6vh'
            src="./coder-bg-Img.png"
            alt="Simulacre Web Logo"
          /> */}

        </nav>
      
      <Navbar />

          {title}

      </header>

      {children}

      <footer>
        
        {/* {'I`m here to stay'} */}

        <div className="text-center">
        <p className="mb-2">
          © 2021 | Coded with
          <span className="align-middle text-lg" role="img" aria-label="coding">
            &nbsp;❤️&nbsp;
          </span>
          by <span className="text-primary font-cursive">Hugues Gouttebroze</span>
        </p>
      </div>

        <Image
            borderRadius="full"
            boxSize="100px"
            src="/logo-simulacre-bg-orange.png"
            alt="Simulacre Web Logo"
        />
        <Image
            borderRadius="full"
            boxSize="300px"
            h='6vh'
            src="./coder-bg-Img.png"
            alt="Simulacre Web Logo"
        />
        </footer>
    </div>
  )
}
